#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "board.h"

typedef enum dir_t {
    LEFT = 1,
    RIGHT,
    UP,
    DOWN,
    UP_LEFT,
    UP_RIGHT,
    DOWN_LEFT,
    DOWN_RIGHT
} DIRECTION;

BOARD* initTable() {
    srand(time(NULL));
    BOARD* b = (BOARD*)malloc(sizeof(BOARD));
    
    int i, j;
    
    //Initialize with "-"
    for(i = 0; i < ROW_NUM; i++)
        for(j = 0; j < COL_NUM; j++)
            b->game[i][j] = '-';

    //Random Start
    if(rand () % 2)
        b->current_player = GREEN;
    else
        b->current_player = RED;

    return b;
}

BOOL isValidMove(BOARD *b, int col) {
    if(b->game[0][col] != '-' || col < 0 || col > 6)
        return FALSE;
    return TRUE;
}

void changePlayer(BOARD *b) {
    if(b->current_player == GREEN)
        b->current_player = RED;
    else
        b->current_player = GREEN;
}

void makeMove(BOARD *b, int col) {
    int i;
    if(isValidMove(b, col)) {
        i = 0;
        while(b->game[i][col] == '-')
            i++;
        i--;
        if(b->current_player == GREEN)
            b->game[i][col] = 'G';
        else
            b->game[i][col] = 'R';
        if(!gameWon(b))
            changePlayer(b);
        else {
            if(b->current_player == GREEN)
                printf("\nGAME WON BY GREEN!\n");
            else
                printf("\nGAME WON BY RED!\n");
        }
    }
}

void printTable(BOARD *b) {
 int i, j;
 if(b->current_player == GREEN)
    printf("CURRENT PLAYER: GREEN\n");
 else
    printf("CURRENT PLAYER: RED\n");
 printf("CURRENT TABLE:\n");
 for(j = 0; j < COL_NUM; j++)
    printf("--%d-", j + 1);
 printf("-\n");
 for(i = 0; i < ROW_NUM; i++) {
    for(j = 0; j < COL_NUM; j++)
        printf("| %c ", b->game[i][j]);
    printf("|\n");
 }
 for(j = 0; j < COL_NUM; j++)
    printf("--%d-", j + 1);
 printf("-\n");
 printf("\n");
}

/** CHECK FUNCTIONS **/
BOOL checkDirection(BOARD *b, int row, int col, char c, DIRECTION d) {
    if(b->game[row][col] == c)
        switch(d) {
            case UP:
                if(b->game[row][col] == b->game[row-1][col] && 
                b->game[row][col] == b->game[row-2][col] &&
                b->game[row][col] == b->game[row-3][col])
                    return TRUE;
                return FALSE;
                break;
            case DOWN:
                if(b->game[row][col] == b->game[row+1][col] && 
                b->game[row][col] == b->game[row+2][col] &&
                b->game[row][col] == b->game[row+3][col])
                    return TRUE;
                return FALSE;
                break;
            case LEFT:
                if(b->game[row][col] == b->game[row][col-1] && 
                b->game[row][col] == b->game[row][col-2] &&
                b->game[row][col] == b->game[row][col-3])
                    return TRUE;
                return FALSE;
                break;
            case RIGHT:
                if(b->game[row][col] == b->game[row][col+1] && 
                b->game[row][col] == b->game[row][col+2] &&
                b->game[row][col] == b->game[row][col+3])
                    return TRUE;
                return FALSE;
                break;
            case DOWN_LEFT:
                if(b->game[row][col] == b->game[row+1][col-1] && 
                b->game[row][col] == b->game[row+2][col-2] &&
                b->game[row][col] == b->game[row+3][col-3])
                    return TRUE;
                return FALSE;
                break;
            case DOWN_RIGHT:
                if(b->game[row][col] == b->game[row+1][col+1] && 
                b->game[row][col] == b->game[row+2][col+2] &&
                b->game[row][col] == b->game[row+3][col+3])
                break;
            case UP_LEFT:
                if(b->game[row][col] == b->game[row-1][col-1] && 
                b->game[row][col] == b->game[row-2][col-2] &&
                b->game[row][col] == b->game[row-3][col-3])
                    return TRUE;
                return FALSE;
                break;
            case UP_RIGHT:
                if(b->game[row][col] == b->game[row-1][col+1] && 
                b->game[row][col] == b->game[row-2][col+2] &&
                b->game[row][col] == b->game[row-3][col+3])
                    return TRUE;
                return FALSE;
                break;
        }
    return FALSE;
}

BOOL boardFull(BOARD *b) {
    int i, j;
    for(i = 0; i < ROW_NUM; i++)
        for(j = 0; j < COL_NUM; j++)
            if(b->game[i][j] == '-')
                return FALSE;
    return TRUE;
}

BOOL gameWon(BOARD *b) {
    int i, j;
    char c = 'G';

    if(b->current_player == RED)
        c = 'R';
    for(i = 0; i < ROW_NUM; i++)
        for(j = 0; j < COL_NUM; j++) {
            if(i <= 2)
                if(checkDirection(b, i, j, c, DOWN))
                    return TRUE;
            if(i >= 3)
                if(checkDirection(b, i, j, c, UP))
                    return TRUE;
            if(j <= 3)
                if(checkDirection(b, i, j, c, RIGHT))
                    return TRUE;
            if(j >= 3)
                if(checkDirection(b, i, j, c, LEFT))
                    return TRUE;
            if(i <= 2 && j <= 3)
                if(checkDirection(b, i, j, c, DOWN_RIGHT))
                    return TRUE;
            if(i <= 2 && j >= 3)
                if(checkDirection(b, i, j, c, DOWN_LEFT))
                    return TRUE;
            if(i >= 3 && j <= 3)
                if(checkDirection(b, i, j, c, UP_RIGHT))
                    return TRUE;
            if(i >= 3 && j >= 3)
                if(checkDirection(b, i, j, c, UP_LEFT))
                    return TRUE;
        }
    return FALSE;
}

void cleanupTable(BOARD *b) { 
    free(b);
}