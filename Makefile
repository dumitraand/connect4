build: conn4

conn4: conn4.c board.c
	gcc -Wall conn4.c board.c -o conn4

.PHONY: clean

clean:
	rm -rf *.o conn4
