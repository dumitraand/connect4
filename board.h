#ifndef __BOARD_H__
#define __BOARD_H__

//Standard connect 4
#define ROW_NUM 6
#define COL_NUM 7

typedef enum bool_t {
    FALSE = 0,
    TRUE = 1
} BOOL;

typedef enum player_t {
    RED = 0,
    GREEN = 1
} PLAYER;

typedef struct board_t { 
    char game[ROW_NUM][COL_NUM];
    PLAYER current_player;
} BOARD;

//Initializes the table
BOARD* initTable();

//Checks if game is Won
BOOL gameWon(BOARD*);

//Checks if move is Valid
BOOL isValidMove(BOARD*, int);

//Check if game is Draw
BOOL boardFull(BOARD*);

//Clears the board;
void cleanupTable(BOARD*);

//Prints Table
void printTable(BOARD*);

//Makes a Move
void makeMove(BOARD*, int);
#endif