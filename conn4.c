#include <stdio.h>
#include <stdlib.h>

#include "board.h"

int main() {
    BOARD* board = initTable();
    int move;
    while(!gameWon(board) || !boardFull(board)) {
        system("clear");
        printTable(board);
        printf("Choose the column you wish to place your token (1 - 7): ");
        scanf("%d", &move);
        while(!isValidMove(board, move - 1)) {
            printf("Not a valid move! Please choose again (1 - 7): ");
            scanf("%d", &move);
        }
        makeMove(board, move - 1);
    }
    cleanupTable(board);
    
    return 0;
}